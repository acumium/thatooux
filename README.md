# About OOUX and Acumium

### What is OOUX?

OOUX is a powerful design methodology that has become Acumium's default approach for all website and software application projects that have a moderate to high level of complexity. 

OOUX helps us define and design usable, consistent applications that naturally align with end users’ mental models of real world, concrete objects in a given problem domain. OOUX offers a way to design systems that make sense to how human brains work: object-orientedly. 

We love this methodology because it creates extremely strong bones, shared language, and structural design longevity. We have found no downsides to it. The business, project, and customer dividends are significant. It is a critical component to how we do business and requirements discovery, UX/UI design, data modeling, and content modeling.

Whether you are a developer, a designer, a content modeler, or someone who has influence over digital teams, OOUX offers a new and exciting option to add to your toolkit that will allow you to deliver better digital projects, quicker and more efficiently, and at a higher level of quality than ever before.

### Why is it important?

**For businesses and project teams**

- Creates a shared language, making team communication more effective
- Defines the deep structure of a system and how objects in that system interact; allows a much more comprehensive view of the system as a whole
- Declutters and simplifies designs, eliminating extraneous design elements
- Helps our system scale, increasing maintainability and reducing regression issues that otherwise might be introduced by updates or new features
- Increases portability of our system, allowing devs to build better APIs
- Means fewer things to build; economizes development

**For end users**

- Matches the application structure and interactions to how users think about the subject matter, significantly increasing usability
- Reduces friction and creates tight, goal-oriented flows that allow users to take action on key conversion points exactly when they make the most sense
- Helps users “get to content through content” by providing crucial contextual navigation (within content, outside of main nav) by exposing all the different ways objects naturally relate to each other

### Interested in learning more?

[Caroline Sober-James](https://www.acumium.com/meet-the-team/caroline-sober-james), Acumium's Director of User Experience, is at the forefront of understanding and leading the adoption of this exciting methodology. She works closely and regularly with Sophia Prater, the creator of OOUX, and is a [Certified OOUX Strategist](https://www.objectorientedux.com/strategists/CarolineSober-James).

To learn how OOUX can benefit your enterprise or client projects, [contact us today](https://www.acumium.com/contact-us).

## About Acumium
Since 2001, [Acumium](https://www.acumium.com) has provided strategy, design, and handcrafted technology. We drive digital transformation for companies big and small, using our capabilities in website and application design, custom software development, user experience, digital marketing, and IT support. Our team of experts have helped hundreds of brands be their best, create loyal customers, and solve complex problems.

 Every day, we bring our Midwestern work ethic, smart ideas, and dependable expertise to the table. We’re proud to have helped over 200 businesses grow. We care about doing what’s right, and love to make a difference for companies who want to do the same.

[www.acumium.com](https://www.acumium.com)

[hello@acumium.com](mailto:hello@acumium.com)

[608-310-9700](tel:608-310-9700)
