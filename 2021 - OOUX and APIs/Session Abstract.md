# When OOUX meets API design: Love at first "site"
## [Download the slide deck](OOUX + APIs - Love at First Site 2021.pdf)
## THAT Conference 2021 Abstract
### [Caroline Sober-James](https://that.us/members/caroline)

**Day:** Tue, July 27 

**Time:** 10:30 AM 

**Tags:** ooux, api design, user experience, interface design

**Description**
There’s a digital design methodology gaining steam that you've likely never heard of, but that could make a big difference in your API design being clean, intuitive, and effortlessly usable for devs who use it. It’s called object-oriented user experience (OOUX).

OOUX is a design power tool that helps us define usable, consistent products that naturally align with end users’ mental models. Similar to its namesake, object-oriented programming, it asks us to define the objects in the real-world problem domain and design the information and relationships in each object before designing how the user might manipulate them. The highly structured nature of OOUX means it directly contributes to clean data modeling and highly portable systems that translate well to exposure via API. It's a powerful tool and it pays dividends fast.

In this session, you’ll get a high-level overview of what OOUX is and why it’s a game-changer, a discussion of the parallels and complementary concerns between OOUX and API design, and an idea of how API design planning could be represented in an OOUX data model.

## Some OOUX resources 
[“Three Reasons OOUX is a Game Changer for Your Digital Design Projects”](https://www.acumium.com/blog/three-reasons-ooux-game-changer-digital-design-projects) by Caroline Sober-James

[OOUX Podcast: Episode 17 — Using OOUX in the wild](https://open.spotify.com/episode/1XaH66M4UHCKXYhZqIwRyv?si=GWmGJBGxRHGVYekk4_COQw&dl_branch=1
) with Caroline Sober-James

[“Object-Oriented UX”](https://alistapart.com/article/object-oriented-ux/) by Sophia Prater

[“The Object-Oriented User”](https://blog.prototypr.io/the-object-oriented-user-52c5bbdb246c) by Sophia Prater

[“UX for Lizard Brains”](https://alistapart.com/article/ux-for-lizard-brains/) by Sophia Prater

[“OOUX: A Foundation for Interaction Design”](https://alistapart.com/article/ooux-a-foundation-for-interaction-design/) by Sophia Prater    
