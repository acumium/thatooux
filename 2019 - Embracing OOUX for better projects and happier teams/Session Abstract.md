# Embracing Object-Oriented UX for better projects (and happier teams)
## [Download the slide deck](Embracing OOUX for Better Projects and Happier Teams - THAT Conference 2019.pdf)
## THAT Conference 2019 Abstract
### [Caroline Sober-James](https://www.thatconference.com/Speakers/Speaker/cjames)

**Day:** Tue, Aug 6 **Time:** 10:30 AM 

**Level:** 100 
**Primary Category:** UX/UI 
**Secondary Category:** Tools
**Tags:** UX, Process Improvement, UI, OOUX, content modeling, mental models

**Description**
Imagine if designers conversed with you in a way that felt like object-oriented programming. Imagine if they handed off a design where, page after page, the objects you needed to code were edged in neon, so clearly defined they popped off the wireframe or comp. Imagine those objects were consistently presented; no one-off cases or guesswork required. Imagine you could take a design and almost create an ERD or rough out an API with it.  
Well, good news. There’s no need to imagine it. It exists, and it’s called Object-Oriented UX (OOUX). 
OOUX is a design methodology that helps us define usable, consistent products that naturally align with end users’ mental models. Similar to OOP, it asks us to define the objects in the real-world problem domain and design the information and relationships in each object before designing how the user might manipulate them. It's a powerful tool for any digital team, it's relatively easy to do, and it pays dividends fast.
In this talk, you’ll come away with: 

-   A high-level overview of what OOUX is 
-   A basic toolkit for trying it with your team 
-   Our experience-based insights on how it can provide value to: 
    -   **Developers,** because it breaks down design into objects, jumpstarting work in a more natural and familiar way. OOUX also creates a common vocabulary and understanding with designers, increasing cross-team empathy and efficiency, and improving quality.  
    -   **Designers,** because it provides a content-first foundation of consistent object components to work from before designing interactions 
    -   **Clients and stakeholders,** because it offers an easy-to-understand tool for documenting and organizing systems, allowing easier extraction of the “whats” before the "hows"
    -   **Content modelers,** because the objects we define in OOUX overlap significantly with those needed to model structured content 
    -   **Trainers,** because it creates a natural glossary and framework to document systems and interactions in a way that matches human mental models, making training more effective 

Whether you are a developer, a designer, a content modeler, or someone who has influence over digital teams, you’ll come away with a new and exciting option to add to your toolkit that will allow you to deliver better digital projects, quicker and more efficiently, and at a higher level of quality than ever before.

## Good OOUX reading 
["Object Oriented UX"](https://alistapart.com/article/object-oriented-ux/)  by Sophia Voychehovski, 20 October 2015

["OOUX: A Foundation for Interaction Design"](https://alistapart.com/article/ooux-a-foundation-for-interaction-design/)  by Sophia Voychehovski, 19 April 2016

["UX for Lizard Brains"](https://alistapart.com/article/ux-for-lizard-brains/)  by Sophia Voychehovski, 10 October 2017